

#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "SampleHandler/ScanDir.h"
//--- needed for ntuple output definition
#include "EventLoopAlgs/NTupleSvc.h" 
#include "EventLoop/OutputStream.h"
#include "EventLoopGrid/PrunDriver.h"

#include "ExclWW/ExclWW_init.h"
#include "ExclWW/ExclWW_analysis.h"
using namespace std;



int main( int argc, char* argv[] ) {


  // Take the submit directory from the input if provided:
  std::string submitDir = "subDir2";
  if( argc > 1 ) submitDir = argv[ 3 ];



  // Set up the job for xAOD access:
  xAOD::Init().ignore();


  // Construct the samples to run on:
  SH::SampleHandler sh;

/* SH::scanRucio (sh, "mc15_13TeV:mc15_13TeV.363686.HerwigppEvtGen_BudnevQED_ggTOmumu_6M18_LeptonFilter.merge.AOD.e4717_s2726_r7725_r7676");
 SH::scanRucio (sh, "mc15_13TeV:mc15_13TeV.363687.HerwigppEvtGen_BudnevQED_ggTOmumu_18M60_LeptonFilter.merge.AOD.e4717_s2726_r7725_r7676");
 SH::scanRucio (sh, "mc15_13TeV:mc15_13TeV.363688.HerwigppEvtGen_BudnevQED_ggTOmumu_60M200_LeptonFilter.merge.AOD.e4717_s2726_r7725_r7676");
 SH::scanRucio (sh, "mc15_13TeV:mc15_13TeV.363689.HerwigppEvtGen_BudnevQED_ggTOmumu_200M_LeptonFilter.merge.AOD.e4717_s2726_r7725_r7676");
//*/
  // SH::scanRucio(sh, "mc15_13TeV.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.merge.AOD.e4616_s2726_r7772_r7676" );
  // SH::scanRucio(sh, "user.fnechans.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.merge.DAOD_STDM7.e4616_s2726_r7772_r7676_v5rel_EXT0/");

//SH::scanRucio(sh, "user.fnechans.missing.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv_TMP0");

 // SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.341079.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH125_WWlvlv_EF_15_5.merge.AOD.e3871_s2608_s2183_r7772_r7676" ); 
//  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361078.Sherpa_CT10_ggllvvNoHiggs.merge.AOD.e3911_s2608_s2183_r7772_r7676" ); 

//  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361601.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvll_mll4.merge.AOD.e4054_s2608_s2183_r7725_r7676");
//  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361607.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqll_mll20.merge.AOD.e4054_s2608_s2183_r7725_r7676");
//  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.merge.AOD.e4054_s2608_s2183_r7725_r7676");
//  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361610.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.merge.AOD.e4054_s2608_s2183_r7725_r7676");

 //  SH::scanRucio(sh, "mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676" );
//     SH::scanRucio(sh, "mc15_13TeV.410015.PowhegPythiaEvtGen_P2012_Wt_dilepton_top.merge.AOD.e3753_s2608_s2183_r7725_r7676" );
 //  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.410016.PowhegPythiaEvtGen_P2012_Wt_dilepton_antitop.merge.AOD.e3753_s2608_s2183_r7725_r7676" ); 
  // SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.DAOD_STDM7.grp16_v01_p2667" );
  // SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.DAOD_STDM4.grp16_v01_p2667");
 //SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.AOD.t0pro20_v01");
 // SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodB.physics_Main.PhysCont.AOD.t0pro20_v01");
 //  SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodC.physics_Main.PhysCont.AOD.t0pro20_v01");
//   SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodD.physics_Main.PhysCont.AOD.t0pro20_v01");
  // SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodE.physics_Main.PhysCont.AOD.t0pro20_v01");
 //  SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodF.physics_Main.PhysCont.AOD.t0pro20_v01");
 //  SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodG.physics_Main.PhysCont.AOD.t0pro20_v01");
 //  SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodI.physics_Main.PhysCont.AOD.t0pro20_v01");
 //  SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodK.physics_Main.PhysCont.AOD.t0pro20_v01");
 
 
//  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.AOD.e3601_s2726_r7725_r7676");
// SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e3601_s2576_s2132_r7725_r7676");
//  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_s2576_s2132_r7725_r7676");

//  SH::scanRucio(sh, "mc15_13TeV.361665.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_10M60.merge.AOD.e4770_s2726_r7772_r7676");
  //  SH::scanRucio(sh, "mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.AOD.e4770_s2726_r7772_r7676");
//  SH::scanRucio(sh, "mc15_13TeV.361669.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_10M60.merge.AOD.e4770_s2726_r7772_r7676");

// SH::scanRucio(sh, "mc15_13TeV.363123.MGPy8EG_N30NLO_Zmumu_Ht0_70_CVetoBVeto.merge.AOD.e4649_s2726_r7772_r7676");
 //SH::scanRucio(sh, "mc15_13TeV.363126.MGPy8EG_N30NLO_Zmumu_Ht70_140_CVetoBVeto.merge.AOD.e4649_s2726_r7772_r7676");
  /* SH::scanRucio(sh, "mc15_13TeV.363124.MGPy8EG_N30NLO_Zmumu_Ht0_70_CFilterBVeto.merge.AOD.e4649_s2726_r7772_r7676");
   SH::scanRucio(sh, "mc15_13TeV.363125.MGPy8EG_N30NLO_Zmumu_Ht0_70_BFilter.merge.AOD.e4649_s2726_r7772_r7676");
    SH::scanRucio(sh, "mc15_13TeV.363129.MGPy8EG_N30NLO_Zmumu_Ht140_280_CVetoBVeto.merge.AOD.e4649_s2726_r7772_r7676");
    SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361634.MadGraphPythia8EvtGen_A14NNPDF23LO_Zmumu_lowMll_Np1.merge.AOD.e4442_s2608_s2183_r7772_r7676");*/
//SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV...merge.AOD.e4442_s2608_s2183_r7772_r7676");
/*   SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361510.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_Np0.merge.AOD.e3898_s2608_s2183_r7725_r7676");
   SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361633.MadGraphPythia8EvtGen_A14NNPDF23LO_Zmumu_lowMll_Np0.merge.AOD.e4442_s2608_s2183_r7772_r7676");
   SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361638.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_lowMll_Np0.merge.AOD.e4442_s2608_s2183_r7772_r7676");
 //*/
   /*
      SH::scanRucio(sh, "user.fnechans.363686.HerwigppEvtGen_BudnevQED_ggTOmumu_6M18_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_EXT0");
  SH::scanRucio(sh, "user.fnechans.363687.HerwigppEvtGen_BudnevQED_ggTOmumu_18M60_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_EXT0");
  *///SH::scanRucio(sh, "user.fnechans.363688.HerwigppEvtGen_BudnevQED_ggTOmumu_60M200_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_EXT0");
 // SH::scanRucio(sh, "user.fnechans.363689.HerwigppEvtGen_BudnevQED_ggTOmumu_200M_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_EXT0");
  //SH::scanRucio(sh, "user.fnechans.363123.MGPy8EG_N30NLO_Zmumu_Ht0_70_CVetoBVeto.merge.AOD.e4649_s2726_r7772_r7676_test3_EXT0");
  //SH::scanRucio(sh, "user.fnechans.363126.MGPy8EG_N30NLO_Zmumu_Ht70_140_CVetoBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_EXT0");
//*/

   // SH::scanRucio(sh, "user.fnechans.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.merge.DAOD_STDM7.e4616_s2726_r7772_r7676_v1_EXT0/");
  //  SH::scanRucio(sh, "user.fnechans.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.merge.DAOD_STDM7.e4616_s2726_r7772_r7676_v3_EXT0/");
   //
   // SH::scanRucio(sh, "user.fnechans.00302872.physics_Main.merge.DAOD_STDM7.f716_m1620_v1_EXT0/");
   // SH::scanRucio(sh, "user.fnechans.00302872.physics_Main.merge.DAOD_STDM7.f716_m1620_v2_EXT0/");
    //SH::scanRucio(sh, "data16_13TeV:data16_13TeV.00302872.physics_Main.merge.AOD.f716_m1620_tid08914529_00");
  //  SH::scanRucio(sh, "user.fnechans.00302872.physics_Main.merge.DAOD_STDM7.f716_m1620_v4_EXT0/");
    
  //   SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.DAOD_HIGG3D1.grp16_v01_p2667");

//  SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.DAOD_STDM3.grp16_v01_p2667");
 // SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp16_v01_p2667");
//  SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp16_v01_p2667_p2689");


// SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.AOD.t0pro20_v01");
//  SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodF.physics_Main.PhysCont.AOD.t0pro20_v01");

//////////////////////////////////////////////////////////////////////////////////////
////-----FINAL SAMPLES----////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


  // ALL DATA!
  //  SH::scanRucio(sh, "user.fnechans.EXCL.WW.STDM7.13TeV.v01");

  // SIGNAL!
  //  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363723.Herwig7EvtGen_BudnevQED_WWlvlv_LeptonFilter.merge.DAOD_STDM7.e5631_s2726_r7772_r7676_p2990");
//  SH::scanRucio(sh, "mc15_13TeV.363723.Herwig7EvtGen_BudnevQED_WWlvlv_LeptonFilter.merge.AOD.e5631_s2726_r7772_r7676");

//  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_STDM7.e3601_s2576_s2132_r7725_r7676_p2990");
//  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.DAOD_STDM7.e3601_s2576_s2132_r7725_r7676_p2990");
//  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361667.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_10M60.merge.DAOD_STDM7.e4770_s2726_r7772_r7676_p2990");
/*  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363692.HerwigppEvtGen_BudnevQED_ggTOtautau_60M200_LeptonFilter.merge.DAOD_STDM7.e4731_s2726_r7725_r7676_p2990");
  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363690.HerwigppEvtGen_BudnevQED_ggTOtautau_6M18_LeptonFilter.merge.DAOD_STDM7.e4731_s2726_r7725_r7676_p2990");
  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363679.Pythia8EvtGen_NNPDF23QED_ggTOtautau_6M18_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p2990");
  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363681.Pythia8EvtGen_NNPDF23QED_ggTOtautau_60M200_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p2990");
  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363691.HerwigppEvtGen_BudnevQED_ggTOtautau_18M60_LeptonFilter.merge.DAOD_STDM7.e4731_s2726_r7725_r7676_p2990");
/// SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361669.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_10M60.merge.DAOD_STDM7.e4770_s2726_r7772_r7676_p2990");
  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363680.Pythia8EvtGen_NNPDF23QED_ggTOtautau_18M60_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p2990");
 // SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361665.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_10M60.merge.DAOD_STDM7.e4770_s2726_r7772_r7676_p2990");
  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363682.Pythia8EvtGen_NNPDF23QED_ggTOtautau_200_LeptonFilter.merge.DAOD_STDM7.e4913_s2726_r7773_r7676_p2990");
 // SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_STDM7.e3601_s2726_r7725_r7676_p2990");
  SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363693.HerwigppEvtGen_BudnevQED_ggTOtautau_200M_LeptonFilter.merge.DAOD_STDM7.e4731_s2726_r7725_r7676_p2990");
*/ 

// SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.merge.DAOD_STDM7.e4616_s2726_r7772_r7676_p2990");
// SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_STDM7.e3698_s2608_s2183_r7725_r7676_p2987");
//SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.341079.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH125_WWlvlv_EF_15_5.merge.DAOD_STDM7.e3871_s2608_s2183_r7772_r7676_p2990");
//SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.361078.Sherpa_CT10_ggllvvNoHiggs.merge.DAOD_STDM7.e3911_s2608_s2183_r7772_r7676_p2990");
//*/


/*
   SH::scanRucio(sh, "mc15_13TeV.361500.MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_Np0.merge.DAOD_STDM7.e3898_s2608_s2183_r7725_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361501.MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_Np1.merge.DAOD_STDM7.e3898_s2608_s2183_r7725_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361502.MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_Np2.merge.DAOD_STDM7.e3898_s2608_s2183_r7725_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361503.MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_Np3.merge.DAOD_STDM7.e3898_s2608_s2183_r7725_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361504.MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_Np4.merge.DAOD_STDM7.e3898_s2608_s2183_r7725_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361628.MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_lowMll_Np0.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361629.MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_lowMll_Np1.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361630.MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_lowMll_Np2.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361631.MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_lowMll_Np3.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361632.MadGraphPythia8EvtGen_A14NNPDF23LO_Zee_lowMll_Np4.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2990");
// */
/*
   SH::scanRucio(sh, "mc15_13TeV.361638.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_lowMll_Np0.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361639.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_lowMll_Np1.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361640.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_lowMll_Np2.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361641.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_lowMll_Np3.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361642.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_lowMll_Np4.merge.DAOD_STDM7.e4442_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361510.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_Np0.merge.DAOD_STDM7.e3898_s2608_s2183_r7725_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361511.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_Np1.merge.DAOD_STDM7.e3898_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361512.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_Np2.merge.DAOD_STDM7.e3898_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361513.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_Np3.merge.DAOD_STDM7.e3898_s2608_s2183_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.361514.MadGraphPythia8EvtGen_A14NNPDF23LO_Ztautau_Np4.merge.DAOD_STDM7.e3898_s2608_s2183_r7772_r7676_p2990");
// */
/*
   SH::scanRucio(sh, "mc15_13TeV.363123.MGPy8EG_N30NLO_Zmumu_Ht0_70_CVetoBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363124.MGPy8EG_N30NLO_Zmumu_Ht0_70_CFilterBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363125.MGPy8EG_N30NLO_Zmumu_Ht0_70_BFilter.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363126.MGPy8EG_N30NLO_Zmumu_Ht70_140_CVetoBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363127.MGPy8EG_N30NLO_Zmumu_Ht70_140_CFilterBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363128.MGPy8EG_N30NLO_Zmumu_Ht70_140_BFilter.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363129.MGPy8EG_N30NLO_Zmumu_Ht140_280_CVetoBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363130.MGPy8EG_N30NLO_Zmumu_Ht140_280_CFilterBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363131.MGPy8EG_N30NLO_Zmumu_Ht140_280_BFilter.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363132.MGPy8EG_N30NLO_Zmumu_Ht280_500_CVetoBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363133.MGPy8EG_N30NLO_Zmumu_Ht280_500_CFilterBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363134.MGPy8EG_N30NLO_Zmumu_Ht280_500_BFilter.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363135.MGPy8EG_N30NLO_Zmumu_Ht500_700_CVetoBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363136.MGPy8EG_N30NLO_Zmumu_Ht500_700_CFilterBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363137.MGPy8EG_N30NLO_Zmumu_Ht500_700_BFilter.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363725.MGPy8EG_N30NLO_Zmumu_lowMll_Ht0_70_CVetoBVeto.merge.DAOD_STDM7.e4863_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363726.MGPy8EG_N30NLO_Zmumu_lowMll_Ht0_70_CFilterBVeto.merge.DAOD_STDM7.e4863_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363727.MGPy8EG_N30NLO_Zmumu_lowMll_Ht0_70_BFilter.merge.DAOD_STDM7.e4887_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363728.MGPy8EG_N30NLO_Zmumu_lowMll_Ht70_140_CVetoBVeto.merge.DAOD_STDM7.e4863_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363729.MGPy8EG_N30NLO_Zmumu_lowMll_Ht70_140_CFilterBVeto.merge.DAOD_STDM7.e4887_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363730.MGPy8EG_N30NLO_Zmumu_lowMll_Ht70_140_BFilter.merge.DAOD_STDM7.e4887_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363731.MGPy8EG_N30NLO_Zmumu_lowMll_Ht140_280_CVetoBVeto.merge.DAOD_STDM7.e4863_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363732.MGPy8EG_N30NLO_Zmumu_lowMll_Ht140_280_CFilterBVeto.merge.DAOD_STDM7.e4863_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363733.MGPy8EG_N30NLO_Zmumu_lowMll_Ht140_280_BFilter.merge.DAOD_STDM7.e4887_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363734.MGPy8EG_N30NLO_Zmumu_lowMll_Ht280_500_CVetoBVeto.merge.DAOD_STDM7.e4863_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363735.MGPy8EG_N30NLO_Zmumu_lowMll_Ht280_500_CFilterBVeto.merge.DAOD_STDM7.e4887_s2726_r7772_r7676_p2990"); 
   SH::scanRucio(sh, "mc15_13TeV.363736.MGPy8EG_N30NLO_Zmumu_lowMll_Ht280_500_BFilter.merge.DAOD_STDM7.e4887_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363737.MGPy8EG_N30NLO_Zmumu_lowMll_Ht500_700_CVetoBVeto.merge.DAOD_STDM7.e4863_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363738.MGPy8EG_N30NLO_Zmumu_lowMll_Ht500_700_CFilterBVeto.merge.DAOD_STDM7.e4863_s2726_r7772_r7676_p2990");
   SH::scanRucio(sh, "mc15_13TeV.363739.MGPy8EG_N30NLO_Zmumu_lowMll_Ht500_700_BFilter.merge.DAOD_STDM7.e4863_s2726_r7772_r7676_p2990");
// */

/*SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363683.HerwigppEvtGen_BudnevQED_ggTOee_18M60_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_p2990");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363684.HerwigppEvtGen_BudnevQED_ggTOee_60M200_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_p2990");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363685.HerwigppEvtGen_BudnevQED_ggTOee_200M_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_p2990");


SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363690.HerwigppEvtGen_BudnevQED_ggTOtautau_6M18_LeptonFilter.merge.DAOD_STDM7.e4731_s2726_r7725_r7676_p2990");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363691.HerwigppEvtGen_BudnevQED_ggTOtautau_18M60_LeptonFilter.merge.DAOD_STDM7.e4731_s2726_r7725_r7676_p2990");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363692.HerwigppEvtGen_BudnevQED_ggTOtautau_60M200_LeptonFilter.merge.DAOD_STDM7.e4731_s2726_r7725_r7676_p2990");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363693.HerwigppEvtGen_BudnevQED_ggTOtautau_200M_LeptonFilter.merge.DAOD_STDM7.e4731_s2726_r7725_r7676_p2990");
// */

/*
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363686.HerwigppEvtGen_BudnevQED_ggTOmumu_6M18_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_p2990");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363687.HerwigppEvtGen_BudnevQED_ggTOmumu_18M60_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_p2990");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363688.HerwigppEvtGen_BudnevQED_ggTOmumu_60M200_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_p2990");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363689.HerwigppEvtGen_BudnevQED_ggTOmumu_200M_LeptonFilter.merge.DAOD_STDM7.e4717_s2726_r7725_r7676_p2990"); 

SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363676.Pythia8EvtGen_NNPDF23QED_ggTOmumu_18M60_LeptonFilter.merge.AOD.e4913_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363677.Pythia8EvtGen_NNPDF23QED_ggTOmumu_60M200_LeptonFilter.merge.AOD.e4913_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363678.Pythia8EvtGen_NNPDF23QED_ggTOmumu_200_LeptonFilter.merge.AOD.e4913_s2726_r7772_r7676");

SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363698.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOmumu_18M60_LeptonFilter.merge.AOD.e4909_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363699.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOmumu_60M200_LeptonFilter.merge.AOD.e4909_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363700.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOmumu_200M_LeptonFilter.merge.AOD.e4909_s2726_r7772_r7676");
// */

/*SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363695.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOee_60M200_LeptonFilter.merge.AOD.e4909_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363694.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOee_18M60_LeptonFilter.merge.AOD.e4909_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363674.Pythia8EvtGen_NNPDF23QED_ggTOee_200_LeptonFilter.merge.AOD.e4913_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363673.Pythia8EvtGen_NNPDF23QED_ggTOee_60M200_LeptonFilter.merge.AOD.e4913_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363696.HepMCAsciiEG_Lpair_BSY_SDiss_ggTOee_200M_LeptonFilter.merge.AOD.e4909_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363672.Pythia8EvtGen_NNPDF23QED_ggTOee_18M60_LeptonFilter.merge.AOD.e4913_s2726_r7772_r7676");

SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363680.Pythia8EvtGen_NNPDF23QED_ggTOtautau_18M60_LeptonFilter.merge.AOD.e4913_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363681.Pythia8EvtGen_NNPDF23QED_ggTOtautau_60M200_LeptonFilter.merge.AOD.e4913_s2726_r7772_r7676");
SH::scanRucio(sh, "mc15_13TeV:mc15_13TeV.363682.Pythia8EvtGen_NNPDF23QED_ggTOtautau_200_LeptonFilter.merge.AOD.e4913_s2726_r7772_r7676");

// */
//SH::scanRucio(sh, "mc15_13TeV.363132.MGPy8EG_N30NLO_Zmumu_Ht280_500_CVetoBVeto.merge.DAOD_STDM7.e4649_s2726_r7772_r7676_p2990");
SH::scanRucio(sh, "data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.AOD.repro21_v01");
//SH::scanRucio(sh, "mc16_13TeV:mc16_13TeV.363132.MGPy8EG_N30NLO_Zmumu_Ht280_500_CVetoBVeto.merge.AOD.e4649_s3126_r9364_r9315");
//SH::scanRucio(sh, "data17_13TeV:data17_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM7.grp21_v01_p3213");
//SH::scanRucio(sh, "data17_13TeV:data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM7.grp21_v01_p3213");

//  sh.setMetaString( "nc_grid_filter", "*.physics_Main.merge.AOD.*" );
  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  // job.options()->setDouble (EL::Job::optMaxEvents, 500); //--- for testing only
  job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);

  //---to prevent long processing time for MC15b AOD's (signal)
  //  if( sampleName.find(".AOD.e4717_s2726_r7326_r6282") == std::string::npos ) job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);
  // define an output and an ntuple associated to that output
  EL::OutputStream output  ("myOutput");
  job.outputAdd (output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
  job.algsAdd (ntuple);

  // Add our analysis to the job:
  // // //  MyxAODAnalysis* alg = new MyxAODAnalysis();
  // //   EXCLRunIIAnalysis *alg = new EXCLRunIIAnalysis;
  // //   // later on we'll add some configuration options for our algorithm that go here
  // //   job.algsAdd( alg );
  // // 
  // //   alg->outputName = "myOutput"; // give the name of the output to our algorithm

  //  MyxAODAnalysis* alg = new MyxAODAnalysis();
  ExclWW_init *alg_init = new ExclWW_init();
  job.algsAdd( alg_init );



  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  alg_init->m_hasTrk = true;
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  //alg_init->outputName = "ExclWW_init"; // give the name of the output to our algorithm

  ExclWW_analysis *alg_analysis = new ExclWW_analysis();
  job.algsAdd( alg_analysis );
  // alg_analysis->outputName = "ExclWW_analysis"; // give the name of the output to our algorithm
  // Run grid:
  EL::PrunDriver driver;
/*  driver.options()->setString( EL::Job::optGridMaxNFilesPerJob, "1" );
  driver.options()->setString( EL::Job::optGridNFiles, "10" );// */
    driver.options()->setString( EL::Job::optGridMergeOutput, "1" );
 //  driver.options()->setString( EL::Job::optGridSite, "PRAGUELCG2_LOCALGROUPDISK" );
 // driver.options()->setString("nc_outputSampleName", "user.fnechans.EXCLReco.ll.STDM7new.13TeV.v12g");
// driver.options()->setString("nc_outputSampleName", "user.fnechans.EXCL.WW.%in:name[5]%.%in:name[6]%.13TeV.v1p3_26");
  //  driver.options()->setString("nc_outputSampleName", "user.fnechans.EXCL.WW.%in:name[3]%.%in:name[5]%.13TeV.v1p3_NW");
  driver.options()->setString("nc_outputSampleName", "user.fnechans.EXCL.WW.%in:name[2]%.%in:name[3]%.13TeV.21.v1a");    
  //   driver.options()->setString("nc_outputSampleName", "user.fnechans.EXCLReco.ll.test.v04");
  driver.submitOnly( job, submitDir );
  // */
  // Run local:
  // Run the job using the local/direct driver:
  //  EL::DirectDriver driver;
  // driver.submit( job, submitDir );



  return 0;
}
