#ifndef MyAnalysis_MyAnalysis_H
#define MyAnalysis_MyAnalysis_H

#include <EventLoop/Algorithm.h>


// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

// EDM includes:
#include "xAODEventInfo/EventInfo.h"

#include <TH1F.h>


class MyAnalysis : public EL::Algorithm
{
   public:
      // this is a standard algorithm constructor
      //MyAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
      MyAnalysis ();

      // these are the functions inherited from Algorithm
      virtual EL::StatusCode setupJob (EL::Job& job);
      virtual EL::StatusCode fileExecute ();
      virtual EL::StatusCode histInitialize ();
      virtual EL::StatusCode changeInput (bool firstFile);
      virtual EL::StatusCode initialize ();
      virtual EL::StatusCode execute ();
      virtual EL::StatusCode postExecute ();
      virtual EL::StatusCode finalize ();
      virtual EL::StatusCode histFinalize ();


   private:


      template<class T>
         void Book( T *h ) 
         {
            h->Sumw2();
            book ( *h );
         }

      void FillTracks();

      bool m_isMC;
      bool h_booked;
      // event variables
      xAOD::TEvent* m_event; //!
      const xAOD::EventInfo* m_eventInfo; //!

      ClassDef(MyAnalysis, 1);
};

#endif
